﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Npgsql;
using SproApi.Model;


namespace SproApi.Controllers
{
    public class SprintbacklogsController : Controller
    {
        // GET api/values
        [EnableCors("CorsPolicy")]
        [HttpGet]
        public ActionResult Get()
        {
            String connectionString = "User ID=postgres;Password=postgres;Server=localhost;Port=5432;Database=spro_db;Integrated Security=true;Pooling=true;";
            using (NpgsqlConnection conn = new NpgsqlConnection(connectionString))
            {
                string query = "SELECT * FROM public.\"Sprint_backlog\"";
                NpgsqlCommand command = new NpgsqlCommand(query, conn);

                try
                {
                    conn.Open();
                    NpgsqlDataReader reader = command.ExecuteReader();


                    DataTable dt = new DataTable();
                    dt.Load(reader);


                    reader.Close();
                    return Content(JsonConvert.SerializeObject(dt), "application/json");
                }
                catch (Exception ex)
                {
                    return Content("111 " + ex.Message);
                }
            }

        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            String connectionString = "User ID=postgres;Password=postgres;Server=localhost;Port=5432;Database=spro_db;Integrated Security=true;Pooling=true;";
            using (NpgsqlConnection conn = new NpgsqlConnection(connectionString))
            {
                string query = "SELECT * FROM public.\"Sprint_backlog\" WHERE ID_SPRINT = " + id;
                NpgsqlCommand command = new NpgsqlCommand(query, conn);

                try
                {
                    conn.Open();
                    NpgsqlDataReader reader = command.ExecuteReader();


                    DataTable dt = new DataTable();
                    dt.Load(reader);


                    reader.Close();
                    return Content(JsonConvert.SerializeObject(dt), "application/json");
                }
                catch (Exception ex)
                {
                    return Content("111 " + ex.Message);
                }
            }
        }


        // POST api/values
        [HttpPost]
        public void Post(Sprint_backlog value)
        {
            String connectionString = "User ID=postgres;Password=postgres;Server=localhost;Port=5432;Database=spro_db;Integrated Security=true;Pooling=true;";
            using (NpgsqlConnection conn = new NpgsqlConnection(connectionString))
            {
                string query = "INSERT INTO public.\"Sprint_backlog\"(complejidad, fecha_inicio, fecha_fin, creado, modificado, id_tarea, id_asignado, \"SprintId_spr\", \"Estado_Id_es\" )" +
                    "VALUES (" + value.complejidad + ",'" + value.fecha_inicio + "','" + value.fecha_fin + "','" + value.creado + "','" + value.modificado + "'," + value.id_tarea
                    + ",'" + value.id_asignado + "'," + value.id_sprint + "," + value.id_estado + ")";
                NpgsqlCommand command = new NpgsqlCommand(query, conn);

                try
                {
                    conn.Open();
                    NpgsqlDataReader reader = command.ExecuteReader();
                    reader.Close();
                }
                catch (Exception ex)
                {
                    Console.WriteLine("111 " + ex.Message);
                }
            }
        }
        // PUT api/values/5
        [HttpPut]
        public void Put([FromBody]Sprint_backlog value)
        {
            String connectionString = "User ID=postgres;Password=postgres;Server=localhost;Port=5432;Database=spro_db;Integrated Security=true;Pooling=true;";
            using (NpgsqlConnection conn = new NpgsqlConnection(connectionString))
            {
                string query = "UPDATE public.\"Sprint_backlog\" SET  complejidad=" + value.complejidad + ",fecha_inicio=" + value.fecha_inicio + ", fecha_fin='" + value.fecha_fin + "',modificado='" + value.modificado +
                     "',\"SprintId_spr\"" + value.id_sprint + ",\"Estado_Id_es\"" + value.id_estado +" WHERE id_spr=" + value.id_sb;
                NpgsqlCommand command = new NpgsqlCommand(query, conn);

                try
                {
                    conn.Open();
                    NpgsqlDataReader reader = command.ExecuteReader();
                    reader.Close();

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }


    }
}