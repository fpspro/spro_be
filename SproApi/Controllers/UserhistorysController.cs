﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Npgsql;
using System;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using SproApi.Model;

namespace SproApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserhistorysController : Controller
    {
        // GET api/values
        [EnableCors("CorsPolicy")]
        [HttpGet]
        public ActionResult Get()
        {
            String connectionString = "User ID=postgres;Password=postgres;Server=localhost;Port=5432;Database=spro_db;Integrated Security=true;Pooling=true;";
            using (NpgsqlConnection conn = new NpgsqlConnection(connectionString))
            {
                string query = "SELECT * FROM public.\"User_history\"";
                NpgsqlCommand command = new NpgsqlCommand(query, conn);

                try
                {
                    conn.Open();
                    NpgsqlDataReader reader = command.ExecuteReader();


                    DataTable dt = new DataTable();
                    dt.Load(reader);


                    reader.Close();
                    return Content(JsonConvert.SerializeObject(dt), "application/json");
                }
                catch (Exception ex)
                {
                    return Content("111 " + ex.Message);
                }
            }

        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            String connectionString = "User ID=postgres;Password=postgres;Server=localhost;Port=5432;Database=spro_db;Integrated Security=true;Pooling=true;";
            using (NpgsqlConnection conn = new NpgsqlConnection(connectionString))
            {
                string query = "SELECT * FROM public.\"User_history\" WHERE ID_PROYECTO = " + id;
                NpgsqlCommand command = new NpgsqlCommand(query, conn);

                try
                {
                    conn.Open();
                    NpgsqlDataReader reader = command.ExecuteReader();


                    DataTable dt = new DataTable();
                    dt.Load(reader);


                    reader.Close();
                    return Content(JsonConvert.SerializeObject(dt), "application/json");
                }
                catch (Exception ex)
                {
                    return Content("111 " + ex.Message);
                }
            }
        }


        // POST api/values
        [HttpPost]
        public void Post(User_history value)
        {
            String connectionString = "User ID=postgres;Password=postgres;Server=localhost;Port=5432;Database=spro_db;Integrated Security=true;Pooling=true;";
            using (NpgsqlConnection conn = new NpgsqlConnection(connectionString))
            {
                string query = "INSERT INTO public.\"User_history\"(titulo, descripcion, tiempo_estimado, creado, modificado, id_proyecto)"+
                    "VALUES ('" + value.titulo + "','" + value.descripcion + "'," + value.tiempo_estimado + ",'" + value.creado + "','" + value.modificado + "'," + value.id_proyecto  + ")";
                NpgsqlCommand command = new NpgsqlCommand(query, conn);

                try
                {
                    conn.Open();
                    NpgsqlDataReader reader = command.ExecuteReader();
                }
                catch (Exception ex)
                {
                    Console.WriteLine("111 " + ex.Message);
                }
            }
        }
        // PUT api/values/5
        [HttpPut]
        public void Put([FromBody]User_history value)
        {
            String connectionString = "User ID=postgres;Password=postgres;Server=localhost;Port=5432;Database=spro_db;Integrated Security=true;Pooling=true;";
            using (NpgsqlConnection conn = new NpgsqlConnection(connectionString))
            {
                string query = "UPDATE public.\"User_history\" SET  titulo='" + value.titulo + "',tiempo_estimado=" + value.tiempo_estimado + ", descripcion='" + value.descripcion +
                    "' WHERE id_us=" + value.id_us;
                NpgsqlCommand command = new NpgsqlCommand(query, conn);

                try
                {
                    conn.Open();
                    NpgsqlDataReader reader = command.ExecuteReader();
                    reader.Close();

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
