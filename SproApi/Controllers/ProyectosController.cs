﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Npgsql;
using SproApi.Model;

namespace SproApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProyectosController : Controller
    {
        // GET api/values
        [EnableCors("CorsPolicy")]
        [HttpGet]
        public ActionResult Get()
        {
            String connectionString = "User ID=postgres;Password=postgres;Server=localhost;Port=5432;Database=spro_db;Integrated Security=true;Pooling=true;";
            using (NpgsqlConnection conn = new NpgsqlConnection(connectionString))
            {
                string query = "SELECT * FROM public.\"Proyecto\"";
                NpgsqlCommand command = new NpgsqlCommand(query, conn);

                try
                {
                    conn.Open();
                    NpgsqlDataReader reader = command.ExecuteReader();


                    DataTable dt = new DataTable();
                    dt.Load(reader);
                    /* d.Id = reader[0].ToString(); // Probably needs fixing
                     d.UserName = reader[1].ToString(); // Probably needs fixing
                     d.Email = reader[3].ToString(); ; // Probably needs fixing
                     result.Add(d);*/


                    reader.Close();
                    return Content(JsonConvert.SerializeObject(dt), "application/json");
                    //return JsonConvert.DeserializeObject<data>(reader.re);
                }
                catch (Exception ex)
                {
                    return Content("111 " + ex.Message);
                }
            }

        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public void Post(Proyecto value)
        {
            String connectionString = "User ID=postgres;Password=postgres;Server=localhost;Port=5432;Database=spro_db;Integrated Security=true;Pooling=true;";
            using (NpgsqlConnection conn = new NpgsqlConnection(connectionString))
            {
                string query = "INSERT INTO public.\"Proyecto\"(nombre, anho, fecha_inicio, fecha_fin, creado, modificado, descripcion)"+
                    "VALUES ('"+value.nombre+"',"+ value.anho+ ",'"+ value.fecha_inicio+ "','"+value.fecha_fin+"','" + value.creado +"','"+ value.modificado + "','" + value.descripcion+"')";
                NpgsqlCommand command = new NpgsqlCommand(query, conn);

                try
                {
                    conn.Open();
                    NpgsqlDataReader reader = command.ExecuteReader();
                    reader.Close();
                }
                catch (Exception ex)
                {
                    Console.WriteLine("111 " + ex.Message);
                }
            }
        }
        // PUT api/values/5
        [HttpPut]
        public void Put([FromBody]Proyecto value)
        {
            String connectionString = "User ID=postgres;Password=postgres;Server=localhost;Port=5432;Database=spro_db;Integrated Security=true;Pooling=true;";
            using (NpgsqlConnection conn = new NpgsqlConnection(connectionString))
            {
                string query = "UPDATE public.\"Proyecto\" SET  nombre='" + value.nombre + "',anho=" + value.anho + ", fecha_inicio='" + value.fecha_inicio + "',fecha_fin='" + value.fecha_fin +
                    "',modificado='" + value.modificado + "',descripcion='" + value.descripcion + "' WHERE id_proyecto=" + value.id_proyecto;
                NpgsqlCommand command = new NpgsqlCommand(query, conn);

                try
                {
                    conn.Open();
                    NpgsqlDataReader reader = command.ExecuteReader();
                    reader.Close();

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}