﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Npgsql;
using SproApi.Model;

namespace SproApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {

        // GET api/values
        [EnableCors("CorsPolicy")]
        [HttpGet]
        public ActionResult Get()
        {
            String connectionString = "User ID=postgres;Password=postgres;Server=localhost;Port=5432;Database=spro_db;Integrated Security=true;Pooling=true;";
            using (NpgsqlConnection conn = new NpgsqlConnection(connectionString))
            {
                string query = "SELECT * FROM public.\"AspNetUsers\"";
                NpgsqlCommand command = new NpgsqlCommand(query, conn);

                try
                {
                    conn.Open();
                    NpgsqlDataReader reader = command.ExecuteReader();

                    // In this part below, I want the SqlDataReader  to 
                    // read all of the records from database returned, 
                    // and I want the result to be returned as Array or 
                    // Json type, but I don't know how to write this part
                        DataTable dt = new DataTable();
                        dt.Load(reader);
                       /* d.Id = reader[0].ToString(); // Probably needs fixing
                        d.UserName = reader[1].ToString(); // Probably needs fixing
                        d.Email = reader[3].ToString(); ; // Probably needs fixing
                        result.Add(d);*/
                    

                    reader.Close();
                    return Content(JsonConvert.SerializeObject(dt), "application/json");
                    //return JsonConvert.DeserializeObject<data>(reader.re);
                }
                catch (Exception ex)
                {
                    return Content("111 " + ex.Message);
                }
            }
            

        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public void Post(string value)
        {
            Console.WriteLine("algo");
        }

        // PUT api/values/5
        [HttpPut]
        public void Put([FromBody]dynamic value)
        {
            var algo = value;
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }

}
