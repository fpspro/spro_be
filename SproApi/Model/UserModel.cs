﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SproApi.Model
{
    public class UserModel
    {
        public String Id { get; set; }
        public String UserName { get; set; }
        public String Email { get; set; }
    }
}
