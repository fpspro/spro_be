﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SproApi.Model
{
    public class Sprint_backlog
    {
        public Int32 id_sb { get; set; }
        public Int32 complejidad { get; set; }
        public DateTime fecha_inicio { get; set; }
        public DateTime fecha_fin { get; set; }
        public DateTime creado { get; set; }
        public DateTime modificado { get; set; }
        public Nullable<int> id_tarea { get; set; }
        public Nullable<int> id_sprint { get; set; }
        public Nullable<int> id_estado { get; set; }
        public String id_asignado { get; set; }
    }
}
