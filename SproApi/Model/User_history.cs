﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SproApi.Model
{
    public class User_history
    {
        public Int32 id_us { get; set; }
        public String titulo { get; set; }
        public String descripcion { get; set; }
        public Int32 tiempo_estimado { get; set; }
        public DateTime creado { get; set; }
        public DateTime modificado { get; set; }
        public Int32 id_proyecto { get; set; }
    }
}
