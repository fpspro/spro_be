﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SproApi.Model
{
    public class Sprint
    {
        public Int32 id_spr { get; set; }
        public String titulo { get; set; }
        public DateTime fecha_inicio { get; set; }
        public DateTime fecha_fin { get; set; }
        public DateTime creado { get; set; }
        public DateTime modificado { get; set; }
        public String descripcion { get; set; }
    }
}
